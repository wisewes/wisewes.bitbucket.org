// main.js

$(document).ready(function () {
  'use strict';

  console.log('loaded');

  //commonly used selectors
  var $nav = $('nav');
  var $navLi = $('nav ul li');
  var $jumper = $('.jump-to-top');

  /**
   * UIHelpler - module for common UI tasks
   */
  var UIHelper = function () {

    return {

      /**
       * jump to very top
       */
      jumpToTop: function () {

        $('body, html').animate({ scrollTop: "0px" }, 500);
      },

      /**
       * jump to specific element
       */
      jumpToElement: function (elem) {

        //apply adjustment to section's top value whether or not the menu is fully or collapsed
        var navHeight = $($nav).height();
        var adjustment = (navHeight > 32) ? 64 : 0;

        $('body, html').animate({
          scrollTop: $(elem).offset().top - adjustment
        }, 500, function () {
          });
      },

      /**
       * set active to correct nav li item
       */
       toggleActiveClassToLi: function(liElem) {

        if ($(liElem).hasClass('active')) {
          return;
        } else {
          $($navLi).removeClass('active');
          $(liElem).addClass('active');
        }
       }
    };
  } ();



  /*************************************************
   * Event Handlers
   ************************************************/

  /**
   * respond to nav li - click event
   */
  $('body').on('click', 'nav a', function (e) {

    var elem = $(this).attr('href');
    UIHelper.jumpToElement(elem);
  });

  /**
   * respond to jump-to-top - click event
   */
  $($jumper).on('click', function (e) {

    UIHelper.jumpToTop();
  });


  /**
   * respond to scrolling sections into view and updating UI - scroll event
   */
  $(window).on('scroll', function () {

    var currentPosition = $(window).scrollTop();
    var viewPortHeight = $(window).height();
    var viewPortWidth = $(window).width();
    var navHeight = parseInt($('nav').height());


    //section triggers
    var sectProjects = parseInt($('#sect-projects').position().top);
    var sectKnowledge = parseInt($('#sect-knowledge').position().top);
    var sectLearning = parseInt($('#sect-learning').position().top);
    var sectResources = parseInt($('#sect-resources').position().top);
    var sectContact = parseInt($('#sect-contact').position().top);

    //check position to determine if back-to-top should be shown
    if (currentPosition <= viewPortHeight) {
      $($jumper).fadeOut();
    } else {
      $($jumper).fadeIn();
    }

    //check width of viewport; small devices will not toggle sticky class on nav
    if(viewPortWidth <= 480) {
      return;
    }

    //toggle sticky nav on and off base on height
    if (currentPosition >= navHeight) {
      $($nav).addClass('sticky');
    } else {
      $($nav).removeClass('sticky');
      $($navLi).removeClass('active');
    }

    //toggle active class on each menu item when it's corresponding sect is in viewport
    if (currentPosition >= sectProjects) {

      UIHelper.toggleActiveClassToLi($navLi[0]);
    }

    if (currentPosition >= sectKnowledge) {

      UIHelper.toggleActiveClassToLi($navLi[1]);
    }

    if (currentPosition >= sectLearning) {

      UIHelper.toggleActiveClassToLi($navLi[2]);
    }

    if (currentPosition >= sectResources) {

      UIHelper.toggleActiveClassToLi($navLi[3]);
    }

    if (currentPosition >= sectContact) {

      UIHelper.toggleActiveClassToLi($navLi[4]);
    }
  });



  /**
   * animation for section - learning
   */
  var $blocks = $('#sect-learning .block').not('.blank');

  var animateLearning = function() {

    $($blocks).removeClass('animate-flipX animate-flipY');

    var randomIndex = Math.floor(Math.random() * $blocks.length);
    var block =  $blocks[randomIndex];

    if(randomIndex % 2 === 0) {
      $(block).addClass('animate-flipX');
    } else {
      $(block).addClass('animate-flipY');
    }
  };

  setInterval(animateLearning, 4000);



});
