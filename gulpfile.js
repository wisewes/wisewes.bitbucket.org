var gulp = require('gulp');
var jshint = require('gulp-jshint');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');
var html5Lint = require('gulp-html5-lint');


gulp.task('jshint', function() {
  gulp.src('./js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('minify', function() {
  gulp.src('./css/**/*.css')
    .pipe(minify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css/min'));
});

gulp.task('html5-lint', function() {
  gulp.src('*.html')
    .pipe(html5Lint());
});

gulp.task('default', ['jshint', 'minify', 'html5-lint'], function() {});
