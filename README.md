# Portfolio
### Wes Wise

This repo contains dev porftolio and is currently hosted on bitbucket's own static-page hosting.

# What's Accomplished
* hand-coded single-page html markup
* hand-coded a 12-column responsive grid based on percentages
* design uses a modern, flat structure based on a color swatch provided by Kuler
* takes advantage of some current CSS3 features such as animations, tranforms/transitions without going overboard
* contains subfolders that contain a few projects that are also static in nature
* more to come and this will evolve as time goes on

# Portfolio access
[http://wisewes.bitbucket.org](http://wisewes.bitbucket.org)
