$(document).ready(function() {
	'use strict';
		
	//for 500px	
	var Keys = {
		consumer: 'IFWtKHLbcZudyc8LcuwNrKkL4QqgkJ9x5oyAWFHB',
		sdk_key: 'f443b3c125bf48528251df295887404c9b8257fd'
	};
	
	
	/******************************************
	 *  MODULE - wrapper for 500px api
	 *******************************************/
	var PhotosAPI = function() {
		
		//init 500px
		function init500API() {
			
			_500px.init({
				sdk_key: Keys.sdk_key
			});
		}
		
		try {
			init500API();
		} catch(e) {
			alert('Unable to set SDK key for 500px API');
		}
			
		return {
			
			/**
			 * get photos with set params
			 */
			getPhotos: function(params) {
				
				if(params.length === 0) {
					params.feature = 'upcoming';
					params.page = 1;
					params.image_size = 2; //size 2 = 140x140px
				}
				
				//found in 500px.js
				_500px.api('/photos', params,
				function(response) {
					if(response.success) {
						AppModule.outputPhotos(response.data.photos);
					} else {
						alert('Unable to complete request: ' + response.status + ' - ' + response.error_message);
					}
				});
			},
			
			/**
			 * search for photos by keyword/term
			 */
			searchPhotos: function(params) {
				
				if(params.length === 0) {
					params.term = '';
					params.page = 1;
					params.image_size = 3 //size 3 = 280x280px
					params.rpp = 50;
				}
				
				_500px.api('/photos/search', params,
					function(response) {
						if(response.success) {
							console.log(response.data.photos);
							
						} else {
							alert('Unable to complete request: ' + response.status + ' - ' + response.error_message);
						}
					});
				}
		};
	}();
	
	
	
	/****************************************************
	 *  Module - for functions for this app to function 
	 *****************************************************/
	var AppModule = function() {
		
		return {
			
			outputPhotos: function(data) {
				
				var output = '';
		
				for(var i = 0; i < data.length; i++) {
					output += '<div class="row"><div class="col s12">';
					output += '<img class="responsive-img" src="' + data[i].image_url + '" alt="' + data[i].name + '">';
					output += "</div></div>";
				}
				
				$('#photos').empty().append(output);
			},
			
			
			//respond to click event
			selectImageClick: function(imgSelector) {
				
				var image = $(imgSelector).clone();
				$('#preview #inner').empty().append(image);
				
				//enable buttons
				$('#filter-buttons a').removeClass('disabled');
				
				//hide undo and modify wrapper
				$('.task-wrapper').fadeOut();
			},
			
			applyFilterClick: function(filterSelector) {
				
				$('.task-wrapper').show();
				
				var filterName = $(filterSelector).text();
				var $image = ('#preview img');
			
				switch(filterName) {
					case 'Blur':
						$($image).addClass('blur-init');
						TemplateModule.loadBlurTemplate();
					break;
					case 'Brightness':
						$($image).addClass('brightness-init');
						TemplateModule.loadBrightnessTemplate();
					break;
					case 'Contrast':
						$($image).addClass('contrast-init');
						TemplateModule.loadContrastTemplate();
					break;
					case 'Drop Shadow':
						$($image).addClass('drop-shadow-init');
						TemplateModule.loadDropShadow();
					break;
					case 'Grayscale':
						$($image).addClass('grayscale-init');
						TemplateModule.loadGrayscaleTemplate();
					break;
					case 'Hue Rotate':
						$($image).addClass('hue-rotate-init');
						TemplateModule.loadHueRotateTemplate();
					break;
					case 'Invert':
						$($image).addClass('invert-init');
						TemplateModule.loadInvertTemplate();
					break;
					case 'Opacity':
						$($image).addClass('opacity-init');
						TemplateModule.loadOpacityTemplate();
					break;
					case 'Saturate':
						$($image).addClass('saturate-init');
						TemplateModule.loadSaturateTemplate();
					break;
					case 'Sepia':
						$($image).addClass('sepia-init');
						TemplateModule.loadSepiaTemplate();
				}
			},
			
			applyFilterReset: function() {
				
				$('#preview img').removeClass().addClass('responsive-img').removeAttr('style');
				$('#filter-buttons a').removeClass('disabled');
				
				$('.task-wrapper').fadeOut();
			}
		};
	}();
	
	
	/****************************************************
	 *  Module -loads fiilter modification forms
	 *****************************************************/
	var TemplateModule = function() {
		
		var $modDiv = $('.filter-modify');
	
		return {
			
			loadBlurTemplate: function() {
				
				var template = '<p>Modify Blur</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="blurMod" min="0" max="10" value="5">';
				template += '</p></form>';
				template += '<p class="info">In pixels</p>';
				
				$($modDiv).empty().append(template);
			},
			
			loadGrayscaleTemplate: function() {
		
				var template = '<p>Modify Grayscale</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="grayscaleMod" min="0" max="100" value="100">';
				template += '</p></form>';
				template += '<p class="info">In percents</p>';
				
				$($modDiv).empty().append(template);
			},
			
			loadBrightnessTemplate: function() {
				
				var template = '<p>Modify Brightness</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="brightnesMod" min="0" max="100 value="50">';
				template += '</p></form>';
				
				$($modDiv).empty().append(template);
			},
			
			loadContrastTemplate: function() {
				
				var template = '<p>Modify Contrast</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="contrastMod" min="0" max="100" value="50">';
				template += '</p></form>';
				template += '<p class="info">In percents</p>';
				
				$($modDiv).empty().append(template);
			},
		
			loadDropShadow: function() {
				
				$($modDiv).empty().append('<p>Coming Soon</p>');
			},
			
			loadHueRotateTemplate: function() {
				
				var template = '<p>Modify Hue Rotation</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="hueRotateMod" min="0" max="360" value="90">';
				template += '</p></form>';
				template += '<p class="info">In degrees: 0 to 360</p>';
				
				$($modDiv).empty().append(template);
			},
			
			loadInvertTemplate: function() {
						
				var template = '<p>Modify Invert</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="invertMod" min="0" max="100" value="50">';
				template +='</p></form>';
				template += '<p class="info">In percents</p>';
				
				$($modDiv).empty().append(template);
			},
			
			loadOpacityTemplate: function() {
				
				var template = '<p>Modify Opacity</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="opacityMod" min="0" max="100" value="50">';
				template +='</p></form>';
				template += '<p class="info">In percents</p>';
				
				$($modDiv).empty().append(template);
			},
			
			loadSaturateTemplate: function() {
				
				var template = '<p>Modify Saturate</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="saturateMod" min="0" max="1000" value="250">';
				template += '</p></form>';
				template += '<p class="info">In percents</p>';
				
				$($modDiv).empty().append(template);
			},
			
			loadSepiaTemplate: function() {
				var template = '<p>Modify Sepia</p>';
				template += '<form action="#"><p class="range-field">';
				template += '<input type="range" id="sepiaMod" min="0" max="100" value="100">';
				template += '</p></form>';
				template += '<p class="info">In percents</p>';
				
				$($modDiv).empty().append(template);
			}
			
		};
	}();
	

	
	
	
	/*******************************************************
	 * EVENT HANDLERS
	 *******************************************************/	

	//click on the photo
	$('body').on('click', '#photos img', function(e) {
		
			AppModule.selectImageClick(this);
	});

	//click filter button
	$('body').on('click', '#filter-buttons a', function(e) {
		e.preventDefault();
		
		$(this).addClass('disabled');
		AppModule.applyFilterClick(this);
	});
	
	//click filter reset button
	$('body').on('click', '.filter-undo-button a', function(e) {
		
		AppModule.applyFilterReset();
	});
	
	//handlers for modifying the filters
	$('body').on('change', '#grayscaleMod', function(e) {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('grayscale-init').css('filter', 'grayscale(' + value + '%)');
	});
	
	$('body').on('change', '#blurMod', function(e) {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('blur-init').css('filter', 'blur(' + value + 'px)');
	});
	
	$('body').on('change', '#brightnesMod', function(e) {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('brigthness-init').css('filter', 'brightness(' + value + ')');
	});
	
	$('body').on('change', '#contrastMod', function(e) {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('contrast-init').css('filter', 'contrast(' + value + '%)');
	});
	
	
	$('body').on('change', '#hueRotateMod', function() {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('hue-rotate-init').css('filter', 'hue-rotate(' + value + 'deg)');
	});
	
	$('body').on('change', '#invertMod', function() {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('invert-init').css('filter', 'invert(' + value + '%)');
	});
	
	
	$('body').on('change', '#opacityMod', function() {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('opacity-init').css('filter', 'opacity(' + value + '%)');
	});
	
	
	$('body').on('change', '#saturateMod', function() {
		var value = $(this).val();
		
		console.log(value);
		
		$('#preview img').removeClass('saturate-init').css('filter', 'saturate(' + value + '%)');
	});
	
	
	$('body').on('change', '#sepiaMod', function() {
		var value = $(this).val();
		console.log(value);
		
		$('#preview img').removeClass('sepia-init').css('filter', 'sepia(' + value + ')%');
	});
	
	
	/*** main */
	function main() {
		
		var params = { term: 'upcoming',  image_size: 440, only: 'Nature', rpp: 10 };
		PhotosAPI.getPhotos(params);
	}

	//run it	
	main();
	
});
